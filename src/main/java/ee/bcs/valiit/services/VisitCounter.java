package ee.bcs.valiit.services;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VisitCounter {

	private static final String SITE_CODE = "VALIIT";
	
	public static String addVisit() {
		if (visitRecordExist()) {
			updateVisitCounter();
		} else {
			addVisitRecord();
		}
		
		return getVisitCount();
	}
	
	private static boolean visitRecordExist() {
		String sql = "SELECT * FROM visits WHERE site_code = '" + SITE_CODE + "'";
		ResultSet result = CompanyService.executeSql(sql);
		try {
			if (result == null || !result.next()) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private static void addVisitRecord() {
		String sql = String.format("INSERT INTO visits (site_code, count) VALUES ('%s', 1)", SITE_CODE);
		CompanyService.executeSql(sql);
	}
	
	private static void updateVisitCounter() {
		String sql = String.format("UPDATE visits SET count = count + 1 WHERE site_code = '%s'", SITE_CODE);
		CompanyService.executeSql(sql);
	}

	private static String getVisitCount() {
		String sql = String.format("SELECT * FROM visits WHERE site_code = '%s'", SITE_CODE);
		ResultSet result = CompanyService.executeSql(sql);
		try {
			if (result != null && result.next()) {
				return result.getString("count");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "0";
	}
	
}
