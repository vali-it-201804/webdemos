package ee.bcs.valiit.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NameService {

	public static List<String> getNames(String startsWith) {

		List<String> namesToReturn = new ArrayList<>();

		String sql = String.format("SELECT * FROM cool_names WHERE name LIKE '%s%%'", startsWith);
		ResultSet nameSet = CompanyService.executeSql(sql);

		if (nameSet != null) {
			try {
				while (nameSet.next()) {
					namesToReturn.add(nameSet.getString("name"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return namesToReturn;
	}

	public static void addName(String name) {
		String sql = String.format("INSERT INTO cool_names (name) VALUES('%s')", name);
		CompanyService.executeSql(sql);
	}

	public static void deleteName(String name) {
		String sql = String.format("DELETE FROM cool_names WHERE name = '%s'", name);
		CompanyService.executeSql(sql);
	}

}
