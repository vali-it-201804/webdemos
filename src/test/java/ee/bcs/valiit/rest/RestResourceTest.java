package ee.bcs.valiit.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import ee.bcs.valiit.model.Company;
import ee.bcs.valiit.model.MessageDTO;
import ee.bcs.valiit.services.CompanyService;
import ee.bcs.valiit.services.FileService;
import ee.bcs.valiit.services.NameService;

public class RestResourceTest extends JerseyTest {

	@Override
	public void setUp() throws Exception {
		
		NameService.deleteName("Kairi");
		NameService.deleteName("Helen");
		
		super.setUp();
	}
	
	@Override
	public void tearDown() throws Exception {
		
		NameService.deleteName("Kairi");
		NameService.deleteName("Helen");
		
		List<Company> companies = CompanyService.getCompanies();
		for (Company company : companies) {
			if (company.getName().startsWith("Marek Test Company")) {
				// Kustutame kettalt ettevõtte logi faili.
				Files.deleteIfExists(Paths.get(FileService.FILE_FOLDER + company.getLogo()));
			}
		}
		
		// Kustutame baasist kõik ettevõtted, mis loodud Unit Testingu raames.
		CompanyService.executeSql("DELETE FROM company WHERE name LIKE 'Marek Test Company%'");
		
		super.tearDown();
	}
	
	@Override
	public Application configure() {
		enable(TestProperties.LOG_TRAFFIC);
		enable(TestProperties.DUMP_ENTITY);
		Map<String, Object> properties = new HashMap<>();
		properties.put("jersey.config.server.provider.packages", "ee.bcs.valiit.rest");
		properties.put("jersey.config.server.provider.classnames",
				"org.glassfish.jersey.media.multipart.MultiPartFeature");
		return new ResourceConfig(RestResource.class).register(MultiPartFeature.class)
				.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, "INFO").addProperties(properties);
	}

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(MultiPartFeature.class);
	}

	@Test
	public void testHiText() {
		Response output = target("/hi/text").request().get();
		System.out.println(output.readEntity(String.class));
		assertEquals("should return status 200", 200, output.getStatus());
	}

	@Test
	public void testHiJsonMapName() {
		Response output = target("/hi/json/map/Marek").request().get();
		@SuppressWarnings("unchecked")
		Map<String, String> x = output.readEntity(Map.class);
		System.out.println(x);
		System.out.println(x.get("greeting"));
		assertEquals("should return status 200", 200, output.getStatus());
	}

	@Test
	public void testHiDto() {
		Response output = target("/hi/json/dto").queryParam("name", "Mary Jones").request().get();
		MessageDTO message = output.readEntity(MessageDTO.class);
		System.out.println(message.getText());
		assertEquals("should return status 200", 200, output.getStatus());
	}

	@Test
	public void testMessage() {
		MessageDTO message = new MessageDTO();
		message.setText("Marek ühiktestib");
		Response output = target("/message").request().post(Entity.entity(message, MediaType.APPLICATION_JSON));
		String responseText = output.readEntity(String.class);
		System.out.println(responseText);
		assertEquals("should return status 200", 200, output.getStatus());
	}

	@Test
	public void testAddCompany() throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test.jpg").getFile());

		FormDataMultiPart multiPart = new FormDataMultiPart();
		multiPart.field("name", "Marek Test Company" + UUID.randomUUID().toString());
		multiPart.field("employee_count", "2222");
		multiPart.field("established", "2018-05-01");

		FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("logo", file, MediaType.APPLICATION_OCTET_STREAM_TYPE);
		multiPart.bodyPart(fileDataBodyPart);

		Response output = target("/add_company").request(MediaType.MULTIPART_FORM_DATA)
				.post(Entity.entity(multiPart, multiPart.getMediaType()));

		String message = output.readEntity(String.class);
		System.out.println(message);
		assertEquals("should return status 200", 200, output.getStatus());
	}
	
	@Test
	public void testRetrieveNames() {
		Response output = target("/get_names").queryParam("starts_with", "U").request().get();
		String[] names = output.readEntity(String[].class);
		assertTrue(names.length == 3);
		
		for(String name : names) {
			assertTrue(name.toUpperCase().startsWith("U"));
		}
	}
	
	@Test
	public void testRetrieveNames2() {
		List<String> names = NameService.getNames("U");
		assertTrue(names.size() == 3);
		
		for(String name : names) {
			assertTrue(name.toUpperCase().startsWith("U"));
		}
	}
	
	@Test
	public void testAddName() {
		NameService.addName("Helen");
		NameService.addName("Kairi");
		
		List<String> listOfHelens = NameService.getNames("Helen");
		List<String> listOfKairis = NameService.getNames("Kairi");
		
		assertTrue(listOfHelens.size() == 1);
		assertTrue(listOfKairis.size() == 1);
		
		assertTrue(listOfHelens.get(0).equals("Helen"));
		assertTrue(listOfKairis.get(0).equals("Kairi"));
	}

}
